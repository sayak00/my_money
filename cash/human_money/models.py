from django.db import models
from django.conf import settings



class Person(models.Model):
    class Meta:
        verbose_name = 'Персона'
        verbose_name_plural = 'Персоны'

    user_profile = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(verbose_name='Имя',max_length=32)
    surname = models.CharField(verbose_name='Фамилия',max_length=32)
    age = models.PositiveIntegerField(verbose_name='Возраст',blank=True, null=True)
    # profile_image = models.ImageField(verbose_name='Фото профиля', upload_to='person_image/')
    current_city = models.CharField(verbose_name='Город проживания', max_length=32)
    plase_of_work = models.CharField(verbose_name='Место работы', max_length=32)
    profession = models.CharField(verbose_name='Профессия', max_length=32, default='SOME STRING')
    budget = models.PositiveIntegerField(verbose_name='Бюджет',default=0)
    saving = models.PositiveIntegerField(verbose_name='Сбережение',default=0)
    costs = models.PositiveIntegerField(verbose_name='Расходы',default=0)
    date = models.DateTimeField(verbose_name='Дата', auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.name

class Costs(models.Model):
    class Meta:
        verbose_name = 'Расход'
        verbose_name_plural = 'Расходы'

    person = models.ForeignKey(Person,on_delete=models.CASCADE,blank=True,null=True,verbose_name='Мои расходы',related_name="person_costs")
    product = models.PositiveIntegerField(verbose_name='Продукты',default=0)
    entertainment = models.PositiveIntegerField(verbose_name='Развлечение',default=0)
    transport = models.PositiveIntegerField(verbose_name='Транспорт',default=0)
    purchuses = models.PositiveIntegerField(verbose_name='Покупки',default=0)
    family = models.PositiveIntegerField(verbose_name='Семья',default=0)
    helth = models.PositiveIntegerField(verbose_name='Здоровье',default=0)
    utilites = models.PositiveIntegerField(verbose_name='Комунальные услуги',default=0)
    date = models.DateTimeField(verbose_name='Дата расходов', auto_now_add=True, blank=True, null=True)

    

class Comment(models.Model):
    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        
    text = models.TextField(verbose_name='Комментарий')
    comment_costs = models.ForeignKey(Costs,on_delete=models.CASCADE)

    def __str__(self):
        return self.text


