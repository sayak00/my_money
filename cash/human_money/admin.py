from django.contrib import admin
from human_money.models import Person, Costs, Comment

admin.site.register(Person)
admin.site.register(Costs)
admin.site.register(Comment)

