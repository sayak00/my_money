from rest_framework import serializers
from human_money.models import Person,Costs,Comment


class PersonSerializers(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    costs = serializers.IntegerField(read_only=True)
    class Meta:
        model = Person
        fields = ('user_profile','name','surname','age','current_city','plase_of_work', 'profession', 'budget','saving','costs','date','id')

    def create(self, validated_data):
        person = Person.objects.create(**validated_data)
        person.user_profile = self.context['request_user']
        person.save()
        return person


class CostsSerializers(serializers.ModelSerializer):
    person = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')

    class Meta:
        model = Costs
        fields = ('person', 'product', 'entertainment','transport','purchuses','family','helth','utilites','date','id')

    def create(self, validated_data):
        costs = Costs.objects.create(**validated_data)    
        person = Person.objects.get(user_profile=self.context['request'])
        costs.person = person
        costs.save()
        return costs

    
class CommentSerializers(serializers.ModelSerializer):
    comment_costs = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Comment
        fields = ('comment_costs', 'text', 'id')

    def create(self, validated_data):
        comment = Comment.objects.create(**validated_data)    
        person = Person.objects.get(user_profile=self.context['request'])
        comment.person = person
        comment.save()
        return comment


class PersonDetailSerializers(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=True)
    person_costs = CostsSerializers(many=True, read_only=True)
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    costs = serializers.IntegerField(read_only=True)

    class Meta:
        model = Person
        fields = ('user_profile', 'name','surname','age','current_city','plase_of_work', 'profession', 'budget','saving','costs','date','person_costs', 'id')


class CostsDetailSerializers(serializers.ModelSerializer):

    person = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    comment_costs = CommentSerializers(many=True, read_only=True)

    class Meta:
        model = Costs
        fields = ('person', 'product', 'entertainment','transport','purchuses','family','helth','utilites','date','id')
