# Generated by Django 3.0.5 on 2020-04-17 14:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('human_money', '0008_auto_20200416_2207'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='person_comment',
        ),
    ]
