# Generated by Django 3.0.5 on 2020-04-16 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('human_money', '0004_remove_person_profile_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='text',
            field=models.TextField(verbose_name='Комментарий'),
        ),
    ]
