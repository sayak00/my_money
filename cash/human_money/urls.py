from django.urls import path
from human_money.views import PersonView,CostsView,CommentView,PersonDetailView,CostsDetailView


urlpatterns =[
    path('person/',PersonView.as_view({'get':'list','post':'create'})),
    path('person/<int:pk>/',PersonDetailView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('person/costs',CostsView.as_view({'get':'list','post':'create'})),
    path('person/costs/<int:pk>/',CostsDetailView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('person/costs/comment', CommentView.as_view({'get':'list','post':'create'})),
    path('person/costs/comment/<int:pk>/', CommentView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('persons/my/', PersonView.as_view({'get': 'my_person'})),
    path('persons/my/costs/', CostsView.as_view({'get': 'my_costs'})),
    
]