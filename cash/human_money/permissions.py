from rest_framework.permissions import BasePermission
from .models import Person,Costs,Comment

class IsPersonOwnerOrGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET' or request.method == 'POST':
            return True
        else:
            person = Person.objects.get(id=view.kwargs['pk'])
            return request.user == person.user_profile


class IsCostsOwnerOrGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET' or request.method == 'POST':
            return True
        else: 
            costs = Costs.objects.get(id=view.kwargs['pk'])
            person = costs.person
            return request.user == costs.person.user_profile


class IsCommentOwnerOrGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET' or request.method == 'POST':
            return True
        else:
            comment = Comment.objects.get(id=view.kwargs['pk'])
            costs = comment.costs
            return request.user == comment.costs.person.user_profile