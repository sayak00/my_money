from django.shortcuts import render
from datetime import datetime
import django_filters.rest_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework import filters,status
from human_money.models import Person,Costs,Comment
from human_money.serializers import PersonSerializers, CostsSerializers, CommentSerializers, PersonDetailSerializers,CostsDetailSerializers
from rest_framework.response import Response
from collections import OrderedDict
from rest_framework.authentication import TokenAuthentication
from .permissions import IsPersonOwnerOrGet,IsCostsOwnerOrGet,IsCommentOwnerOrGet
from rest_framework.pagination import PageNumberPagination



class Pagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page_count', self.page.paginator.num_pages),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data),
        ]))


class PersonView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes= [IsPersonOwnerOrGet,]
    serializer_class = PersonSerializers
    queryset = Person.objects.all()
    pagination_class = Pagination
    filter_backends = [filters.SearchFilter]
    search_fields = ('name', 'surname', 'age', 'current_city', 'plase_of_work', 'profession', )
    lookup_field = 'pk'

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    def my_person(self,request,*args,**kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CostsView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsCostsOwnerOrGet] 
    serializer_class = CostsSerializers
    pagination_class = Pagination
    lookup_field = 'pk'


    def get_queryset(self):
        queryset = Costs.objects.all()
        order_field = self.request.GET.get('order')
        filter_field = {}

        if self.request.GET.get('product'):
            filter_field['product'] = self.request.GET.get('product')

        if self.request.GET.get('entertainment'):
            filter_field['entertainment'] = self.request.GET.get('entertainment')

        if self.request.GET.get('transport'):
            filter_field['transport'] = self.request.GET.get('transport')

        if self.request.GET.get('purchuses'):
            filter_field['purchuses'] = self.request.GET.get('purchuses')

        if self.request.GET.get('family'):
            filter_field['family'] = self.request.GET.get('family')

        if self.request.GET.get('helth'):
            filter_field['helth'] = self.request.GET.get('helth')

        if self.request.GET.get('utilites'):
            filter_field['utilites'] = self.request.GET.get('utilites')

        if order_field:
            queryset = queryset.order_by(order_field)
        
        if filter_field:
            queryset = queryset.filter(**filter_field)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')

        if start_date and end_date:
            start_date = datetime.strptime('start_date', '%d.%m.%Y')
            end_date = datetime.strptime('end_date', '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)

        return queryset


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context

    def my_costs(self, request, *args, **kwargs):
        person = Person.objects.get(user_profile=request.user)
        instance = Costs.objects.filter(person=person)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)


    def create(self,request,*args,**kwargs):
        # costs = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        person = Person.objects.get(user_profile=request.user)
        costs = serializer.validated_data['utilites']
        serializer.save()
        if person:
            person.budget -= int(request.data['utilites'])
            person.budget -= int(request.data['helth'])
            person.budget -= int(request.data['family'])
            person.budget -= int(request.data['purchuses'])
            person.budget -= int(request.data['transport'])
            person.budget -= int(request.data['entertainment'])
            person.budget -= int(request.data['product'])
            person.costs += int(request.data['utilites'])
            person.costs += int(request.data['helth'])
            person.costs += int(request.data['family'])
            person.costs += int(request.data['purchuses'])
            person.costs += int(request.data['transport'])
            person.costs += int(request.data['entertainment'])
            person.costs += int(request.data['product'])
            # person.costs += int(costs.utilites)
            # person.costs += int(costs.helth)
            # person.costs += int(costs.family)
            # person.costs += int(costs.purchuses)
            # person.costs += int(costs.transport)
            # person.costs += int(costs.entertainment)
            # person.costs += int(costs.product)
            person.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def perform_destroy(self, instance):
        person = instance.person
        if person:
            person.budget += int(instance.utilites)
            person.budget += int(instance.helth)
            person.budget += int(instance.family)
            person.budget += int(instance.purchuses)
            person.budget += int(instance.transport)
            person.budget += int(instance.entertainment)
            person.budget += int(instance.product)
            person.costs -= int(instance.utilites)
            person.costs -= int(instance.helth)
            person.costs -= int(instance.family)
            person.costs -= int(instance.purchuses)
            person.costs -= int(instance.transport)
            person.costs -= int(instance.entertainment)
            person.costs -= int(instance.product)
        person.save()
        instance.delete()


    def update(self, request, *args, **kwargs):
        costs = self.get_object()
        serializer = self.get_serializer(instance=costs, data=request.data)
        serializer.is_valid(raise_exception=True)
        person = Person.objects.get(user_profile=request.user)
        if person:
            person.budget += int(costs.utilites)
            person.budget += int(costs.helth)
            person.budget += int(costs.family)
            person.budget += int(costs.purchuses)
            person.budget += int(costs.transport)
            person.budget += int(costs.entertainment)
            person.budget += int(costs.product)
            person.budget -= int(request.data['utilites'])
            person.budget -= int(request.data['helth'])
            person.budget -= int(request.data['family'])
            person.budget -= int(request.data['purchuses'])
            person.budget -= int(request.data['transport'])
            person.budget -= int(request.data['entertainment'])
            person.budget -= int(request.data['product'])
            person.costs -= int(costs.utilites)
            person.costs -= int(costs.helth)
            person.costs -= int(costs.family)
            person.costs -= int(costs.purchuses)
            person.costs -= int(costs.transport)
            person.costs -= int(costs.entertainment)
            person.costs -= int(costs.product)
            person.costs += int(request.data['utilites'])
            person.costs += int(request.data['helth'])
            person.costs += int(request.data['family'])
            person.costs += int(request.data['purchuses'])
            person.costs += int(request.data['transport'])
            person.costs += int(request.data['entertainment'])
            person.costs += int(request.data['product'])

        person.save()
        serializer.save()  
        return Response(serializer.data,status=status.HTTP_200_OK)


class CommentView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsCommentOwnerOrGet] 
    serializer_class = CommentSerializers
    queryset = Comment.objects.all()
    lookup_field = 'pk'


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context



class PersonDetailView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes= [IsPersonOwnerOrGet]
    serializer_class = PersonDetailSerializers
    queryset = Person.objects.all()
    lookup_field = 'pk'


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context


class CostsDetailView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes= [IsPersonOwnerOrGet]
    serializer_class = PersonDetailSerializers
    queryset = Person.objects.all()
    lookup_field = 'pk'


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context
