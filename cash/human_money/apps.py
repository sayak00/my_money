from django.apps import AppConfig


class HumanMoneyConfig(AppConfig):
    name = 'human_money'
